package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaFilterUser;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import com.atlassian.jira.user.UserUtils;
import com.atlassian.crowd.embedded.api.User;

import java.util.Collection;

public class AddSlaFilterUser extends SlaFilterUserAction {

    public AddSlaFilterUser(SlaService slaService) {
        super(slaService);
    }

    public Collection<User> getAllSystemUsers() {
        return UserUtils.getAllUsers();
    }

    @Override
    public String doExecute() throws Exception {

      // final String filteruser = request.getParameter("filteruser");
      final String filteruser = ((String[]) ActionContext.getParameters().get("filteruser"))[0];

      SlaFilterUser slaFilterUser = this.slaService.createSlaFilterUser(filteruser);

      return getRedirect(VIEW_FILTERUSER_PAGE);
    }

    @Override
    public void doValidation() {
        doFieldValidation();
    }

    public String doAdd() throws Exception {
        return INPUT;
    }

}
