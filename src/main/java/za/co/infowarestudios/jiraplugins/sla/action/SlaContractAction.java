package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaContract;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.web.action.JiraWebActionSupport;

public class SlaContractAction extends JiraWebActionSupport {

  // private static final long serialVersionUID = -2345865120367035004L;

  private final static Logger logger = LoggerFactory.getLogger(SlaContractAction.class);

  final static String VIEW_CONTRACT_PAGE = "ViewSlaContract.jspa";

  protected final SlaService slaService;

  public SlaContractAction(SlaService slaService) {
    this.slaService = checkNotNull(slaService);
  }

  protected void doKeyValidation() {
    this.log.debug("Entering doKeyValidation");
    //final String id = this.request.getParameter("key");
    final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
    if (id == null || id.isEmpty()) {
      // ?key=
      addErrorMessage(getText("sla.form.id.missing"));
    } else {
      try {
        this.slaContract = this.slaService.findSlaContractById(Integer.valueOf(id));
        this.slaContract.getIssuetype();
        this.slaContract.getDescription();
        this.slaContract.getMinslatime();
        this.slaContract.getMaxslatime();
      } catch (NumberFormatException e) {
        // ?key=aaa
        addErrorMessage(getText("sla.form.id.wrong.format"));
      } catch (NullPointerException e) {
        // ?key=111
        addErrorMessage(getText("sla.form.not.found"));
      }

    }
  }

  protected void doFieldValidation() {

    // final String description = ((String[]) ActionContext.getParameters().get("description"))[0];

    // if (description == null || description.isEmpty()) {
    //  addErrorMessage(getText("sla.form.description.missing"));
    //}
    //int maxLength = 50;
    //if (description != null && description.length() > maxLength) {
    //  addErrorMessage(getText("sla.form.description.too.long"));
    //}
  }

  protected SlaContract slaContract;

  public SlaContract getSlaContract() {
    return this.slaContract;
  }

}
