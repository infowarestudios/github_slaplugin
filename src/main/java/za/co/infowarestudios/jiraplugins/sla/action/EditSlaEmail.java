package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.UserUtils;
import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collection;
import java.util.Map;

public class EditSlaEmail extends SlaEmailAction {

  private final GroupManager groupManager;
  private ProjectRoleManager projectRoleManager;

  public EditSlaEmail(SlaService slaService, GroupManager groupManager, ProjectRoleManager projectRoleManager) {
    super(slaService);
    this.groupManager = groupManager;
    this.projectRoleManager = projectRoleManager;
  }

  protected ProjectRoleManager getProjectRoleManager() {
    if (projectRoleManager == null) {
      projectRoleManager = ComponentManager.getComponent(ProjectRoleManager.class);
    }
    return projectRoleManager;
  }

  public Collection<Project> getAllSystemProjects() {
    // ProjectManager projectManager = ComponentManager.getComponentInstanceOfType(ProjectManager.class);

    Collection<Project> allProjects = ComponentAccessor.getProjectManager().getProjectObjects();

    return allProjects;
  }

  public Collection<User> getAllSystemUsers() {
    return UserUtils.getAllUsers();
  }

  public Collection<Group> getAllSystemGroups() {
    Collection<Group> allGroups =  groupManager.getAllGroups();
    return allGroups;
  }

  public Collection<ProjectRole> getProjectRoles() {
    return getProjectRoleManager().getProjectRoles();
  }

  @Override
  protected String doExecute() throws Exception {

    boolean reporter = false;
    boolean project_lead = false;
    boolean component_lead = false;
    boolean all_watchers = false;

    Map actionParams = ActionContext.getParameters();
    if (actionParams.containsKey("reporter")) { reporter = true; }
    if (actionParams.containsKey("componentlead")) { component_lead = true; }
    if (actionParams.containsKey("allwatchers"))  { all_watchers = true; }

    final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
    final String user = ((String[])ActionContext.getParameters().get("user"))[0];
    final String role = ((String[])ActionContext.getParameters().get("role"))[0];
    final String group = ((String[])ActionContext.getParameters().get("group"))[0];

    slaEmail.setProject(project);
    slaEmail.setUser(user);
    slaEmail.setRole(role);
    slaEmail.setGroup(group);
    slaEmail.setReporter(reporter);
    slaEmail.setComponentLead(component_lead);
    slaEmail.setAllWatchers(all_watchers);

    slaEmail.save();

    return getRedirect(VIEW_EMAIL_PAGE);
  }

  public String doEdit() throws Exception {
    doKeyValidation();
    return INPUT;
  }

  @Override
  public void doValidation() {
    doKeyValidation();
    doFieldValidation();
  }

}