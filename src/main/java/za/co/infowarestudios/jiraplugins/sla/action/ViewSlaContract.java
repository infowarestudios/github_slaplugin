package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;

import za.co.infowarestudios.jiraplugins.sla.model.SlaContract;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class ViewSlaContract extends SlaContractAction {

  private final static Logger logger = LoggerFactory.getLogger(ViewSlaContract.class);

  public ViewSlaContract(SlaService slaService) {
    super(slaService);
  }

 // public boolean validLicense() {
 //   logger.info("License check in ViewSLAContract ------------------");
 //   return this.slaService.isLicenseValid();
 // }

  @Override
  protected String doExecute() throws Exception {

    boolean validLicense = slaService.isLicenseValid();

    if (!validLicense) {
      logger.warn("Infoware SLA plugin License is INVALID!");
      return getRedirect(this.slaService.LICENSE_PAGE);
    }

    slaContracts = this.slaService.findAllSlaContracts();
    return SUCCESS;
  }

  public String doSelect() {
    //final String issuetype = request.getParameter("issuetype");
    final String issuetype = ((String[]) ActionContext.getParameters().get("issuetype"))[0];
    slaContracts = this.slaService.findSlaContract(issuetype);
    return SUCCESS;
  }

  List<SlaContract> slaContracts = Collections.emptyList();
  public List<SlaContract> getSlaContracts() {
    return slaContracts;
  }

}
