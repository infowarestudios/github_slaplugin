package za.co.infowarestudios.jiraplugins.sla.scheduling;

import java.util.Date;
import com.atlassian.jira.security.JiraAuthenticationContext;

public interface SlaMonitor {

  void reschedule(long interval);

  public Date getLastRun();

  public void setLastRun(Date lastRun);

  public Date getNextRun();

  public void setNextRun(Date nextRun);

  public long getInterval();

  public JiraAuthenticationContext getJiraAuthenticationContext();

}
