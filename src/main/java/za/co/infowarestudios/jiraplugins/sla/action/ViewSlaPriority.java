package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaPriority;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collections;
import java.util.List;

public class ViewSlaPriority extends SlaPriorityAction {

    public ViewSlaPriority(SlaService slaService) {
        super(slaService);
    }

    @Override
    protected String doExecute() throws Exception {

      boolean validLicense = slaService.isLicenseValid();

      if (!validLicense) {
        return getRedirect(this.slaService.LICENSE_PAGE);
      }

      slaPriorities = this.slaService.findAllSlaPriorities();
      return SUCCESS;
    }

    public String doSelect() {
      //final String issuetype = request.getParameter("issuetype");
      final String priority = ((String[]) ActionContext.getParameters().get("priority"))[0];
      slaPriorities = this.slaService.findSlaPriority(priority);
      return SUCCESS;
    }

    List<SlaPriority> slaPriorities = Collections.emptyList();
    public List<SlaPriority> getSlaPriorities() {
        return slaPriorities;
    }

}