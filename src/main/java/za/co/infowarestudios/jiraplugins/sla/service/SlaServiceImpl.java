package za.co.infowarestudios.jiraplugins.sla.service;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import za.co.infowarestudios.jiraplugins.sla.model.*;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.license.storage.lib.PluginLicenseStoragePluginUnresolvedException;
import com.atlassian.upm.license.storage.lib.ThirdPartyPluginLicenseStorageManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;

import net.java.ao.Query;

public final class SlaServiceImpl implements SlaService {

  private final static Logger logger = LoggerFactory.getLogger(SlaServiceImpl.class);

  private final ActiveObjects ao;
  private final ThirdPartyPluginLicenseStorageManager licenseManager;

  public SlaServiceImpl(ActiveObjects ao, ThirdPartyPluginLicenseStorageManager licenseManager) {
     this.licenseManager = checkNotNull(licenseManager);
      this.ao = checkNotNull(ao);
    }

  public boolean isLicenseValid(){

    // ---------------------------------------
    // !! unlicensed version for internal use !!
    return true;
    // ---------------------------------------

  }

  // !! rename to isLicenseValid for licensed versions !!
  public boolean inactive_isLicenseValid(){
    try
    {

      //Check and see if a license is currently stored.
      //This accessor method can be used whether or not a licensing-aware UPM is present.
      if (this.licenseManager.getLicense().isDefined())
      {
        logger.info("licenseManager.getLicense().isDefined()");
        PluginLicense pluginLicense = licenseManager.getLicense().get();
        //Check and see if the stored license has an error. If not, it is currently valid.
        if (pluginLicense.getError().isDefined())
        {
          logger.info("License is currently stored, but it is invalid.");
          //A license is currently stored, however, it is invalid (e.g. expired or user count mismatch)
          return false;
        }
        else
        {
          logger.info("License is currently stored and it is valid.");
          //A license is currently stored and it is valid.
          return true;
        }
      }
      else
      {
        logger.info("License check #5 ------------------");
        logger.info("No license (valid or invalid) is stored. ");
        //No license (valid or invalid) is stored.
        return false;

      }
    }
    catch (PluginLicenseStoragePluginUnresolvedException e)
    {
      //The current license status cannot be retrieved because the Plugin License Storage plugin is unavailable.
      return false;
    }
  }


  // SLA Contract

    @Override
    public SlaContract createSlaContract(String issuetype, String description, String minslatime, String maxslatime) {

        final SlaContract slaContract = ao.create(SlaContract.class);

        slaContract.setIssuetype(issuetype);
        slaContract.setDescription(description);
        slaContract.setMinslatime(minslatime);
        slaContract.setMaxslatime(maxslatime);

        slaContract.setComplete(false);
        slaContract.save();

        return slaContract;
    }

    @Override
    public SlaContract getSlaContract(String issuetype) {
        SlaContract slaContract = null;

        // final SlaContract[] slaContracts = ao.find(SlaContract.class, Query.select().distinct().where("issuetype = ?", issuetype));
        final SlaContract[] slaContracts = ao.find(SlaContract.class, Query.select().where("ISSUETYPE = ?", issuetype));

        //if (slaContracts.length > 1)
        //{
        //    throw new IllegalStateException("Cannot have two SLAs with same name");
        //}

        slaContract = slaContracts.length > 0 ? slaContracts[0] : null;

        // if (slaContract != null) {
        //  return ao.get(SlaContract.class, slaContract.getID());
        //} else {
        //  return null;
        // }

        return slaContract;
    }

    @Override
    public List<SlaContract> findAllSlaContracts() {
        return newArrayList(ao.find(SlaContract.class));
    }

    @Override
    public List<SlaContract> findSlaContract(String issuetype) {
        return newArrayList(ao.find(SlaContract.class, Query.select().where("ISSUETYPE = ?", issuetype)));
    }

    @Override
    public SlaContract findSlaContractById(int id) {
        return ao.get(SlaContract.class, id);
    }

    @Override
    public void deleteSlaContract(int id) {
        SlaContract slaContract = ao.get(SlaContract.class, id);
        ao.delete(slaContract);
    }

    // Priorities

    @Override
    public SlaPriority createSlaPriority(String priority, String minmax, String slatime) {

        final SlaPriority slaPriority = ao.create(SlaPriority.class);

        slaPriority.setPriority(priority);
        slaPriority.setMinmax(minmax);
        slaPriority.setSlatime(slatime);

        slaPriority.save();

        return slaPriority;
    }

    @Override
    public SlaPriority getSlaPriority(String priority) {
        SlaPriority slaPriority = null;

        // final SlaPriority[] SlaPriorities = ao.find(SlaPriority.class, Query.select().distinct().where("issuetype = ?", issuetype));
        final SlaPriority[] slaPriorities = ao.find(SlaPriority.class, Query.select().where("PRIORITY = ?", priority));

        slaPriority = slaPriorities.length > 0 ? slaPriorities[0] : null;
        return slaPriority;

        // if (slaPriority != null) {
        //  return ao.get(SlaPriority.class, SlaPriority.getID());
        //} else {
        //  return null;
        // }
    }

    @Override
    public List<SlaPriority> findAllSlaPriorities() {
        return newArrayList(ao.find(SlaPriority.class));
    }

    @Override
    public List<SlaPriority> findSlaPriority(String priority) {
        return newArrayList(ao.find(SlaPriority.class, Query.select().where("PRIORITY = ?", priority)));
    }

    @Override
    public SlaPriority findSlaPriorityById(int id) {
        return ao.get(SlaPriority.class, id);
    }

    @Override
    public void deleteSlaPriority(int id) {
        SlaPriority slaPriority = ao.get(SlaPriority.class, id);
        ao.delete(slaPriority);
    }

    // Projects

    @Override
    public SlaProject createSlaProject(String project, String insla, String outsla) {

      final SlaProject slaProject = ao.create(SlaProject.class);

      slaProject.setProject(project);
      slaProject.setInsla(insla);
      slaProject.setOutsla(outsla);
      slaProject.save();

      return slaProject;
    }

    @Override
    public SlaProject getSlaProject(String project) {
        SlaProject slaProject = null;

        final SlaProject[] slaProjects = ao.find(SlaProject.class, Query.select().where("PROJECT = ?", project));

        slaProject = slaProjects.length > 0 ? slaProjects[0] : null;

        return slaProject;

        // return ao.get(SlaProject.class, SlaProject.getID());
    }

    @Override
    public List<SlaProject> findAllSlaProjects() {
        return newArrayList(ao.find(SlaProject.class));
    }

    @Override
    public List<SlaProject> findSlaProject(String project) {
        return newArrayList(ao.find(SlaProject.class, Query.select().where("PROJECT = ?", project)));
    }

    @Override
    public SlaProject findSlaProjectById(int id) {
        return ao.get(SlaProject.class, id);
    }

    @Override
    public void deleteSlaProject(int id) {
        SlaProject slaProject = ao.get(SlaProject.class, id);
        ao.delete(slaProject);
    }

    // Filter User configs

    @Override
    public SlaFilterUser createSlaFilterUser(String filteruser) {

        final SlaFilterUser slaFilterUser = ao.create(SlaFilterUser.class);

        slaFilterUser.setFilteruser(filteruser);
        slaFilterUser.save();

        return slaFilterUser;
    }

    @Override
    public SlaFilterUser getSlaFilterUser(String filteruser) {
        SlaFilterUser slaFilterUser = null;

        final SlaFilterUser[] slaFilterUsers = ao.find(SlaFilterUser.class, Query.select().where("FILTERUSER = ?", filteruser));

        slaFilterUser = slaFilterUsers.length > 0 ? slaFilterUsers[0] : null;

        return slaFilterUser;

        // return ao.get(SlaFilterUser.class, SlaFilterUser.getID());
    }

    @Override
    public List<SlaFilterUser> findAllSlaFilterUsers() {
        return newArrayList(ao.find(SlaFilterUser.class));
    }

    @Override
    public List<SlaFilterUser> findSlaFilterUser(String filteruser) {
        return newArrayList(ao.find(SlaFilterUser.class, Query.select().where("FILTERUSER = ?", filteruser)));
    }

    @Override
    public SlaFilterUser findSlaFilterUserById(int id) {
        return ao.get(SlaFilterUser.class, id);
    }

    @Override
    public SlaFilterUser getFirstSlaFilterUser() {

      SlaFilterUser slaFilterUser = null;

      final SlaFilterUser[] slaFilterUsers = ao.find(SlaFilterUser.class, Query.select().limit(1));

      slaFilterUser = slaFilterUsers.length > 0 ? slaFilterUsers[0] : null;

      return slaFilterUser;

    }

    @Override
    public void deleteSlaFilterUser(int id) {
        SlaFilterUser slaFilterUser = ao.get(SlaFilterUser.class, id);
        ao.delete(slaFilterUser);
    }

  // Emails

  @Override
  public SlaEmail createSlaEmail(String project, String user, String group, String role,
                                 boolean reporter, boolean component_lead, boolean all_watchers ) {

    final SlaEmail slaEmail = ao.create(SlaEmail.class);

    slaEmail.setProject(project);
    slaEmail.setUser(user);
    slaEmail.setGroup(group);
    slaEmail.setRole(role);
    slaEmail.setReporter(reporter);
    slaEmail.setComponentLead(component_lead);
    slaEmail.setAllWatchers(all_watchers);
    slaEmail.save();

    return slaEmail;
  }

  @Override
  public SlaEmail getSlaEmail(String project) {
    SlaEmail slaEmail = null;

    final SlaEmail[] slaEmails = ao.find(SlaEmail.class, Query.select().where("PROJECT = ?", project));

    slaEmail = slaEmails.length > 0 ? slaEmails[0] : null;

    return slaEmail;

    // return ao.get(SlaProject.class, SlaProject.getID());
  }

  @Override
  public List<SlaEmail> findAllSlaEmails() {
    return newArrayList(ao.find(SlaEmail.class));
  }

  @Override
  public List<SlaEmail> findSlaEmail(String project) {
    return newArrayList(ao.find(SlaEmail.class, Query.select().where("PROJECT = ?", project)));
  }

  @Override
  public SlaEmail findSlaEmailById(int id) {
    return ao.get(SlaEmail.class, id);
  }

  @Override
  public void deleteSlaEmail(int id) {
    SlaEmail slaEmail = ao.get(SlaEmail.class, id);
    ao.delete(slaEmail);
  }

}
