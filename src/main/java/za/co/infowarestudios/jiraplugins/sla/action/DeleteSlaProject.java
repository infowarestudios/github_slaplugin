package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

public class DeleteSlaProject extends SlaProjectAction {

    public DeleteSlaProject(SlaService slaService) {
        super(slaService);
    }

    @Override
    protected String doExecute() throws Exception {
      //final String id = request.getParameter("key");

      final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
      this.slaService.deleteSlaProject(Integer.valueOf(id));

      return getRedirect(VIEW_PROJECT_PAGE);
    }

    @Override
    public void doValidation() {
        doKeyValidation();
    }

}
