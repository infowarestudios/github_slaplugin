package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaProject;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import static com.google.common.base.Preconditions.checkNotNull;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class SlaProjectAction extends JiraWebActionSupport {

    final static String VIEW_PROJECT_PAGE = "ViewSlaProject.jspa";

    protected final SlaService slaService;

    public SlaProjectAction(SlaService slaService) {
        this.slaService = checkNotNull(slaService);
    }

    protected void doKeyValidation() {
      this.log.debug("Entering doKeyValidation");
      //final String id = this.request.getParameter("key");
      final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
      if (id == null || id.isEmpty()) {
          // ?key=
          addErrorMessage(getText("sla.form.id.missing"));
      } else {
          try {

            this.slaProject = this.slaService.findSlaProjectById(Integer.valueOf(id));
            this.slaProject.getProject();
            this.slaProject.getInsla();
            this.slaProject.getOutsla();

          } catch (NumberFormatException e) {
              // ?key=aaa
              addErrorMessage(getText("sla.form.id.wrong.format"));
          } catch (NullPointerException e) {
              // ?key=111
              addErrorMessage(getText("sla.form.not.found"));
          }

        }
    }

    protected void doFieldValidation() {
      //final String project = this.request.getParameter("project");
      final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
      if (project == null || project.isEmpty()) {
          addErrorMessage(getText("sla.form.project.missing"));
      }
    }

    protected SlaProject slaProject;

    public SlaProject getSlaProject() {
        return this.slaProject;
    }

}
