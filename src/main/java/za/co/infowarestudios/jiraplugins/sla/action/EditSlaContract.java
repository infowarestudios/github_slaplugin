package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import webwork.action.ActionContext;
import java.util.Collection;

public class EditSlaContract extends SlaContractAction {

  public EditSlaContract(SlaService slaService) {
    super(slaService);
  }

  public Collection<IssueType> getAllIssueTypes() {
      Collection<IssueType> allIssueTypes = ComponentAccessor.getConstantsManager().getAllIssueTypeObjects();
      return allIssueTypes;
  }

  @Override
  protected String doExecute() throws Exception {

    //final String description = request.getParameter("description");
    //final String issuetype = request.getParameter("issuetype");
    //final String minslatime = request.getParameter("minslatime");
    //final String maxslatime = request.getParameter("maxslatime");

    final String description = ((String[])ActionContext.getParameters().get("description"))[0];
    final String issuetype = ((String[])ActionContext.getParameters().get("issuetype"))[0];
    final String minslatime = ((String[])ActionContext.getParameters().get("minslatime"))[0];
    final String maxslatime = ((String[])ActionContext.getParameters().get("maxslatime"))[0];

    slaContract.setIssuetype(issuetype);
    slaContract.setDescription(description);
    slaContract.setMinslatime(minslatime);
    slaContract.setMaxslatime(maxslatime);

    slaContract.save();

    return getRedirect(VIEW_CONTRACT_PAGE);
  }

  public String doEdit() throws Exception {
    doKeyValidation();
    return INPUT;
  }

  @Override
  public void doValidation() {
    doKeyValidation();
    doFieldValidation();
  }

}
