package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
// import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.crowd.embedded.api.User;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collection;

public class EditSlaProject extends SlaProjectAction {

    public EditSlaProject(SlaService slaService) {
        super(slaService);
    }

    public Collection<User> getAllUsers() {
        return UserUtils.getAllUsers();
    }

    public Collection<Project> getAllSystemProjects() {
        // ProjectManager projectManager = ComponentManager.getComponentInstanceOfType(ProjectManager.class);

      Collection<Project> allProjects = ComponentAccessor.getProjectManager().getProjectObjects();

      return allProjects;
    }

    @Override
    protected String doExecute() throws Exception {

      final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
      //final String project = request.getParameter("project");
      final String insla = ((String[])ActionContext.getParameters().get("insla"))[0];
      final String outsla = ((String[])ActionContext.getParameters().get("outsla"))[0];

      slaProject.setProject(project);
      slaProject.setInsla(insla);
      slaProject.setOutsla(outsla);

      slaProject.save();

      return getRedirect(VIEW_PROJECT_PAGE);
    }

    public String doEdit() throws Exception {
        doKeyValidation();
        return INPUT;
    }

    @Override
    public void doValidation() {
        doKeyValidation();
        doFieldValidation();
    }

}
