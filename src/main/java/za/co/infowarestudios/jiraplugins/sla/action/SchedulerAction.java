package za.co.infowarestudios.jiraplugins.sla.action;

import za.co.infowarestudios.jiraplugins.sla.scheduling.SlaMonitor;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;
import za.co.infowarestudios.jiraplugins.sla.util.GlobalSettings;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static com.google.common.base.Preconditions.checkNotNull;

public class SchedulerAction extends JiraWebActionSupport {

  private final SlaMonitor slaMonitor;
  private long interval;
  private final SlaService slaService;

  public SchedulerAction(SlaMonitor slaMonitor, SlaService slaService, PluginSettingsFactory pluginSettingsFactory) {
    this.slaService = checkNotNull(slaService);
    this.slaMonitor = checkNotNull(slaMonitor);
    this.interval = slaMonitor.getInterval();
  }

  @Override
  protected String doExecute() throws Exception {

    boolean validLicense = slaService.isLicenseValid();

    if (!validLicense) {
      return getRedirect(this.slaService.LICENSE_PAGE);
    }

    return SUCCESS;
  }

  public String doReschedule() {
    this.slaMonitor.reschedule(this.interval);
    return getRedirect("SlaScheduler.jspa");
  }

  public long getInterval() {
    return this.interval;
  }

  public void setInterval(long interval) {
    this.interval = interval;
  }

  public Date getLastRun() {
    return this.slaMonitor.getLastRun();
  }

  public Date getNextRun() {
    return this.slaMonitor.getNextRun();
  }

  public SimpleDateFormat getHunDateFormatter() {
    return GlobalSettings.createHunDateFormatter();
  }

}
