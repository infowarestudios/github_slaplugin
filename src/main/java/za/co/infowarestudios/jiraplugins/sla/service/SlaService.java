package za.co.infowarestudios.jiraplugins.sla.service;

import za.co.infowarestudios.jiraplugins.sla.model.*;

import java.util.List;

import com.atlassian.activeobjects.tx.Transactional;

@Transactional
public interface SlaService {

  public boolean isLicenseValid();

  public final static String LICENSE_PAGE = "/plugins/servlet/upm/manage/paid-via-atlassian#manage/za.co.infowarestudios.jiraplugins.infoware-sla-plugin";

  SlaContract createSlaContract(String issuetype, String description, String minslatime, String maxslatime);

  List<SlaContract> findAllSlaContracts();

  List<SlaContract> findSlaContract(String issuetype);

  SlaContract getSlaContract(String issuetype);

  SlaContract findSlaContractById(int id);

  void deleteSlaContract(int id);

  // Priorities

  SlaPriority createSlaPriority(String priority, String minmax, String slatime);

  List<SlaPriority> findAllSlaPriorities();

  List<SlaPriority> findSlaPriority(String issuetype);

  SlaPriority getSlaPriority(String issuetype);

  SlaPriority findSlaPriorityById(int id);

  void deleteSlaPriority(int id);

  // Projects

  SlaProject createSlaProject(String project, String insla, String outsla);

  List<SlaProject> findAllSlaProjects();

  List<SlaProject> findSlaProject(String project);

  SlaProject getSlaProject(String project);

  SlaProject findSlaProjectById(int id);

  void deleteSlaProject(int id);


  // Filter User configs

  SlaFilterUser createSlaFilterUser(String filteruser);

  List<SlaFilterUser> findAllSlaFilterUsers();

  List<SlaFilterUser> findSlaFilterUser(String filteruser);

  SlaFilterUser getSlaFilterUser(String filteruser);

  SlaFilterUser getFirstSlaFilterUser();

  SlaFilterUser findSlaFilterUserById(int id);

  void deleteSlaFilterUser(int id);


  // Email configs

  SlaEmail createSlaEmail(String project, String User, String group, String Role,
                          boolean reporter, boolean component_lead, boolean all_watchers );

  List<SlaEmail> findAllSlaEmails();

  List<SlaEmail> findSlaEmail(String project);

  SlaEmail getSlaEmail(String project);

  SlaEmail findSlaEmailById(int id);

  void deleteSlaEmail(int id);
}