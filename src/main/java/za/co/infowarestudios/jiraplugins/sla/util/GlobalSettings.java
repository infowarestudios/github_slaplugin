package za.co.infowarestudios.jiraplugins.sla.util;

import java.text.SimpleDateFormat;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

public class GlobalSettings {

  private final PluginSettings pluginSettings;

  public GlobalSettings(PluginSettings pluginSettings) {
    this.pluginSettings = pluginSettings;
  }

  public Long loadLong(String key) {
    Object object = this.pluginSettings.get(key);
    if (object == null) {
      return null;
    }
    return Long.valueOf((String) object);
  }

  public Long loadInterval() {
    return this.loadLong("slaContract.monitor.interval");
  }

  public void storeInterval(Long interval) {
    this.pluginSettings.put("slaContract.monitor.interval", String.valueOf(interval));
  }

  public static SimpleDateFormat createHunDateFormatter() {
    return new SimpleDateFormat("yyyy.MM.dd. HH:mm");
  }
}
