package za.co.infowarestudios.jiraplugins.sla.model;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface SlaFilterUser extends Entity {

    public String getFilteruser();
    public void setFilteruser(String filteruser);

}