package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

public class DeleteSlaEmail extends SlaEmailAction {

  public DeleteSlaEmail(SlaService slaService) {
    super(slaService);
  }

  @Override
  protected String doExecute() throws Exception {

    final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
    this.slaService.deleteSlaEmail(Integer.valueOf(id));

    return getRedirect(VIEW_EMAIL_PAGE);
  }

  @Override
  public void doValidation() {
    doKeyValidation();
  }

}
