package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaFilterUser;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import static com.google.common.base.Preconditions.checkNotNull;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class SlaFilterUserAction extends JiraWebActionSupport {

    final static String VIEW_FILTERUSER_PAGE = "ViewSlaFilterUser.jspa";

    protected final SlaService slaService;

    public SlaFilterUserAction(SlaService slaService) {
        this.slaService = checkNotNull(slaService);
    }

    protected void doKeyValidation() {
        this.log.debug("Entering doKeyValidation");

        // final String id = this.request.getParameter("key");
        final String id = ((String[]) ActionContext.getParameters().get("key"))[0];

      if (id == null || id.isEmpty()) {
            // ?key=
            addErrorMessage(getText("sla.form.id.missing"));
        } else {
            try {

                this.slaFilterUser = this.slaService.findSlaFilterUserById(Integer.valueOf(id));
                this.slaFilterUser.getFilteruser();

            } catch (NumberFormatException e) {
                // ?key=aaa
                addErrorMessage(getText("sla.form.id.wrong.format"));
            } catch (NullPointerException e) {
                // ?key=111
                addErrorMessage(getText("sla.form.not.found"));
            }

        }
    }

    protected void doFieldValidation() {
      // final String filteruser = this.request.getParameter("filteruser");

      final String filteruser = ((String[]) ActionContext.getParameters().get("filteruser"))[0];
      if (filteruser == null || filteruser.isEmpty()) {
          addErrorMessage(getText("sla.form.filteruser.missing"));
      }
    }

    protected SlaFilterUser slaFilterUser;

    public SlaFilterUser getSlaFilterUser() {
        return this.slaFilterUser;
    }

}
