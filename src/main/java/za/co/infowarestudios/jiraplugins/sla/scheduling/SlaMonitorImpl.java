package za.co.infowarestudios.jiraplugins.sla.scheduling;

import za.co.infowarestudios.jiraplugins.sla.service.SlaService;
import za.co.infowarestudios.jiraplugins.sla.util.GlobalSettings;

import java.util.Date;
import java.util.HashMap;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;

import com.atlassian.jira.security.JiraAuthenticationContext;

import static com.google.common.base.Preconditions.checkNotNull;

public class SlaMonitorImpl implements SlaMonitor, LifecycleAware {

  private static final Logger logger = LoggerFactory.getLogger(SlaMonitorImpl.class);
  private static final String JOB_NAME = SlaMonitorImpl.class.getName() + ":job";

  private final PluginScheduler pluginScheduler;
  private final SlaService SlaService;
  private final GlobalSettings settings;

  private final JiraAuthenticationContext jiraAuthenticationContext; // provided by SAL

  private final HashMap<String, Object> jobDataMap = new HashMap<String, Object>();

  private Date lastRun;
  private Date nextRun;
  private long interval = 300000;

  public SlaMonitorImpl(PluginScheduler pluginScheduler,
                        SlaService SlaService,
                        PluginSettingsFactory pluginSettingsFactory,
                        JiraAuthenticationContext jiraAuthenticationContext) {

    this.pluginScheduler = checkNotNull(pluginScheduler);

    this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);

    this.SlaService = checkNotNull(SlaService);
    this.settings = new GlobalSettings(pluginSettingsFactory.createGlobalSettings());

    Long loadInterval = this.settings.loadInterval();

    if (loadInterval != null) {
      this.interval = loadInterval;
    } else {
      this.settings.storeInterval(interval);
    }

  }

  public JiraAuthenticationContext getJiraAuthenticationContext() {
        return jiraAuthenticationContext;
    }

  @Override
  public void onStart() {

    this.jobDataMap.put("SlaMonitorImpl:instance", SlaMonitorImpl.this);
    this.jobDataMap.put("SlaService", this.SlaService);

    if (this.interval > 0) {
      schedule();
    }
  }

  @Override
  public void reschedule(long interval) {
    this.interval = interval;
    this.settings.storeInterval(interval);
    this.schedule();
  }

  private void schedule() {
    DateTime startTime = this.lastRun == null ? DateTime.now() : new DateTime(this.lastRun);
    this.pluginScheduler.scheduleJob(JOB_NAME, SlaTask.class, this.jobDataMap, startTime.toDate(), this.interval);
    this.setNextRun(startTime.plus(this.interval).toDate());
    logger.info(String.format("SlaMonitorImpl scheduled to run every %d ms", this.interval));
  }

  @Override
  public void setLastRun(Date lastRun) {
    this.lastRun = lastRun;
  }

  @Override
  public Date getLastRun() {
    return this.lastRun;
  }

  @Override
  public Date getNextRun() {
    return this.nextRun;
  }

  @Override
  public void setNextRun(Date nextRun) {
    this.nextRun = nextRun;
  }

  @Override
  public long getInterval() {
    return this.interval;
  }

}
