package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

public class DeleteSlaFilterUser extends SlaFilterUserAction {

    public DeleteSlaFilterUser(SlaService slaService) {
        super(slaService);
    }

    @Override
    protected String doExecute() throws Exception {

      // final String id = request.getParameter("key");

      final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
      this.slaService.deleteSlaFilterUser(Integer.valueOf(id));

      return getRedirect(VIEW_FILTERUSER_PAGE);
    }

    @Override
    public void doValidation() {
        doKeyValidation();
    }

}
