package za.co.infowarestudios.jiraplugins.sla.model;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface SlaContract extends Entity {

  public String getDescription();
  public void setDescription(String description);

  public String getIssuetype();
  public void setIssuetype(String issuetype);

  public String getMinslatime();
  public void setMinslatime(String minslatime);

  public String getMaxslatime();
  public void setMaxslatime(String maxslatime);

  public void setComplete(boolean complete);

}
