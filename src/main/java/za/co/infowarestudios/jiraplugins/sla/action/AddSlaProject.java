
package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaProject;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collection;

public class AddSlaProject extends SlaProjectAction {

    public AddSlaProject(SlaService slaService) {
        super(slaService);
    }

    public Collection<Project> getAllSystemProjects() {
      Collection<Project> allProjects = ComponentAccessor.getProjectManager().getProjectObjects();
      return allProjects;
    }

    @Override
    public String doExecute() throws Exception {

      final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
      // final String project = request.getParameter("project");
      final String insla = ((String[])ActionContext.getParameters().get("insla"))[0];
      final String outsla = ((String[])ActionContext.getParameters().get("outsla"))[0];

      SlaProject slaProject = this.slaService.createSlaProject(project, insla, outsla);

      return getRedirect(VIEW_PROJECT_PAGE);
    }

    @Override
    public void doValidation() {
        doFieldValidation();
    }

    public String doAdd() throws Exception {
        return INPUT;
    }

}
