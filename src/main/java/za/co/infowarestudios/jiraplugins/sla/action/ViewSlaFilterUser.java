package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaFilterUser;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collections;
import java.util.List;

public class ViewSlaFilterUser extends SlaFilterUserAction {

    public ViewSlaFilterUser(SlaService slaService) {
        super(slaService);
    }

    @Override
    protected String doExecute() throws Exception {

      boolean validLicense = slaService.isLicenseValid();

      if (!validLicense) {
        return getRedirect(this.slaService.LICENSE_PAGE);
      }

        slaFilterUsers = this.slaService.findAllSlaFilterUsers();
        return SUCCESS;
    }

    public String doSelect() {
      //final String filteruser = request.getParameter("filteruser");
      final String filteruser = ((String[]) ActionContext.getParameters().get("filteruser"))[0];
      slaFilterUsers = this.slaService.findSlaFilterUser(filteruser);
      return SUCCESS;
    }

    List<SlaFilterUser> slaFilterUsers = Collections.emptyList();
    public List<SlaFilterUser> getSlaFilterUsers() {
        return slaFilterUsers;
    }

}

