package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaProject;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collections;
import java.util.List;

public class ViewSlaProject extends SlaProjectAction {

    public ViewSlaProject(SlaService slaService) {
        super(slaService);
    }

    @Override
    protected String doExecute() throws Exception {

      boolean validLicense = slaService.isLicenseValid();

      if (!validLicense) {
        return getRedirect(this.slaService.LICENSE_PAGE);
      }

        slaProjects = this.slaService.findAllSlaProjects();
        return SUCCESS;
    }

    public String doSelect() {
      // final String project = request.getParameter("project");
      final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
      slaProjects = this.slaService.findSlaProject(project);
      return SUCCESS;
    }

    List<SlaProject> slaProjects = Collections.emptyList();
    public List<SlaProject> getSlaProjects() {
        return slaProjects;
    }

}
