package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaPriority;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import static com.google.common.base.Preconditions.checkNotNull;
import com.atlassian.jira.web.action.JiraWebActionSupport;

public class SlaPriorityAction extends JiraWebActionSupport {

    final static String VIEW_PRIORITY_PAGE = "ViewSlaPriority.jspa";

    protected final SlaService slaService;

    public SlaPriorityAction(SlaService slaService) {
        this.slaService = checkNotNull(slaService);
    }

    protected void doKeyValidation() {
      this.log.debug("Entering doKeyValidation");
      //  final String id = this.request.getParameter("key");
      final String id = ((String[]) ActionContext.getParameters().get("key"))[0];
      if (id == null || id.isEmpty()) {
          // ?key=
          addErrorMessage(getText("sla.form.id.missing"));
      } else {
          try {

              this.slaPriority = this.slaService.findSlaPriorityById(Integer.valueOf(id));
              this.slaPriority.getPriority();
              this.slaPriority.getMinmax();
              this.slaPriority.getSlatime();

          } catch (NumberFormatException e) {
              // ?key=aaa
              addErrorMessage(getText("sla.form.id.wrong.format"));
          } catch (NullPointerException e) {
              // ?key=111
              addErrorMessage(getText("sla.form.not.found"));
          }

        }
    }

    protected void doFieldValidation() {
      //final String priority = this.request.getParameter("priority");
      final String priority = ((String[])ActionContext.getParameters().get("priority"))[0];
      if (priority == null || priority.isEmpty()) {
          addErrorMessage(getText("sla.form.priority.missing"));
      }
    }

    protected SlaPriority slaPriority;

    public SlaPriority getSlaPriority() {
        return this.slaPriority;
    }

}