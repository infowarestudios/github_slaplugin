package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.component.ComponentAccessor;
import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaPriority;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.crowd.embedded.api.User;

import java.util.Collection;

public class AddSlaPriority extends SlaPriorityAction {


    public AddSlaPriority(SlaService slaService) {
        super(slaService);
    }

    public Collection<Priority> getAllPriorities() {
        Collection<Priority> allPriorities = ComponentAccessor.getConstantsManager().getPriorityObjects();
        return allPriorities;
    }

    public Collection<User> getAllUsers() {
        return UserUtils.getAllUsers();
    }

    @Override
    public String doExecute() throws Exception {

      //  final String priority = request.getParameter("priority");
      //  final String minmax = request.getParameter("minmax");

      final String priority = ((String[]) ActionContext.getParameters().get("priority"))[0];
      final String minmax = ((String[]) ActionContext.getParameters().get("minmax"))[0];
      final String slatime = ((String[]) ActionContext.getParameters().get("slatime"))[0];

      SlaPriority slaPriority = this.slaService.createSlaPriority(priority, minmax, slatime);

      return getRedirect(VIEW_PRIORITY_PAGE);
    }

    @Override
    public void doValidation() {
        doFieldValidation();
    }

    public String doAdd() throws Exception {
        return INPUT;
    }

}