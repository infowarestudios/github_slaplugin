package za.co.infowarestudios.jiraplugins.sla.util;

public class DateStringUtil {

    public String ConvertToStringDuration (long timeValue) {

        String duration = "0m";

        long minutes = timeValue / (60 * 1000) % 60;
        long hours = timeValue / (60 * 60 * 1000) % 24;
        long days = timeValue / (24 * 60 * 60 * 1000);

        duration = days > 0 ? days + "d " : "";
        duration += hours > 0 ? hours + "h " : "";
        duration += minutes > 0 ? minutes + "m" : "";

        return duration;

    }

}
