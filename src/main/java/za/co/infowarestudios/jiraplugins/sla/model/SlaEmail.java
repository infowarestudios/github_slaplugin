package za.co.infowarestudios.jiraplugins.sla.model;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface SlaEmail extends Entity {

  public String getProject();
  public void setProject(String project);

  public String getUser();
  public void setUser(String user);

  public String getGroup();
  public void setGroup(String group);

  public String getRole();
  public void setRole(String role);

  public boolean getReporter();
  public void setReporter(boolean reporter);

  public boolean getComponentLead();
  public void setComponentLead(boolean component_lead);

  public boolean getAllWatchers();
  public void setAllWatchers(boolean all_watchers);


}