package za.co.infowarestudios.jiraplugins.sla.action;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaEmail;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collections;
import java.util.List;

public class ViewSlaEmail extends SlaEmailAction {

  public ViewSlaEmail(SlaService slaService) {
    super(slaService);
  }

  @Override
  protected String doExecute() throws Exception {

    boolean validLicense = slaService.isLicenseValid();

    if (!validLicense) {
      return getRedirect(this.slaService.LICENSE_PAGE);
    }

    slaEmails = this.slaService.findAllSlaEmails();
    return SUCCESS;
  }

  public String doSelect() {
    // final String project = request.getParameter("project");
    final String project = ((String[]) ActionContext.getParameters().get("project"))[0];
    slaEmails = this.slaService.findSlaEmail(project);
    return SUCCESS;
  }

  List<SlaEmail> slaEmails = Collections.emptyList();
  public List<SlaEmail> getSlaEmails() {
    return slaEmails;
  }

}
