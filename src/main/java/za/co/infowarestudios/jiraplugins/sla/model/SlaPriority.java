package za.co.infowarestudios.jiraplugins.sla.model;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface SlaPriority extends Entity {

  public String getPriority();
  public void setPriority(String priority);

  public String getMinmax();
  public void setMinmax(String minmax);

  public String getSlatime();
  public void setSlatime(String slatime);

}
