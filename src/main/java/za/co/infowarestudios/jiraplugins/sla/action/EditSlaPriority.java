package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.priority.Priority;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import webwork.action.ActionContext;
import java.util.Collection;

public class EditSlaPriority extends SlaPriorityAction {

    public EditSlaPriority(SlaService slaService) {
        super(slaService);
    }


    public Collection<Priority> getAllPriorities() {
        Collection<Priority> allPriorities = ComponentAccessor.getConstantsManager().getPriorityObjects();
        return allPriorities;
    }

    @Override
    protected String doExecute() throws Exception {

      final String priority = ((String[]) ActionContext.getParameters().get("priority"))[0];
      final String minmax = ((String[]) ActionContext.getParameters().get("minmax"))[0];
      final String slatime = ((String[]) ActionContext.getParameters().get("slatime"))[0];

      //  final String priority = request.getParameter("priority");
      //  final String minmax = request.getParameter("minmax");

      slaPriority.setPriority(priority);
      slaPriority.setMinmax(minmax);
      slaPriority.setSlatime(slatime);

      slaPriority.save();

      return getRedirect(VIEW_PRIORITY_PAGE);
    }

    public String doEdit() throws Exception {
        doKeyValidation();
        return INPUT;
    }

    @Override
    public void doValidation() {
        doKeyValidation();
        doFieldValidation();
    }

}
