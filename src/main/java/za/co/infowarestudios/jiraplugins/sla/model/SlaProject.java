package za.co.infowarestudios.jiraplugins.sla.model;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface SlaProject extends Entity {

  public String getProject();
  public void setProject(String project);

  public String getInsla();
  public void setInsla(String insla);

  public String getOutsla();
  public void setOutsla(String outsla);

}
