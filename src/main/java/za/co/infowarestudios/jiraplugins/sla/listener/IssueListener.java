package za.co.infowarestudios.jiraplugins.sla.listener;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import za.co.infowarestudios.jiraplugins.sla.model.SlaContract;
import za.co.infowarestudios.jiraplugins.sla.model.SlaProject;
import za.co.infowarestudios.jiraplugins.sla.model.SlaPriority;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;

import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.ModifiedValue;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;

import com.atlassian.activeobjects.external.ActiveObjects;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import za.co.infowarestudios.jiraplugins.sla.util.TimeReader;


public class IssueListener implements InitializingBean, DisposableBean {

  private static final Logger logger = LoggerFactory.getLogger(IssueListener.class);
  private final ActiveObjects ao;

  private final SlaService slaService;

  private final EventPublisher eventPublisher;

  public IssueListener(ActiveObjects ao, EventPublisher eventPublisher, SlaService slaService) {
    // eventPublisher.register(this);    // Demonstration only -- don't do this in real code!

    this.eventPublisher = eventPublisher;

    this.slaService = checkNotNull(slaService);
    this.ao = checkNotNull(ao);
  }

   public String convertToStringDuration (long timeValue) {

      String duration = "0m";

      long minutes = timeValue / (60 * 1000) % 60;
      long hours = timeValue / (60 * 60 * 1000) % 24;
      long days = timeValue / (24 * 60 * 60 * 1000);

      duration = days > 0 ? days + "d " : "";
      duration += hours > 0 ? hours + "h " : "";
      duration += minutes > 0 ? minutes + "m" : "";

      return duration;

   }

  @Override
  public void afterPropertiesSet() throws Exception {
    // register ourselves with the EventPublisher
    eventPublisher.register(this);
  }

  @Override
  public void destroy() throws Exception {
    // unregister ourselves with the EventPublisher
    eventPublisher.unregister(this);
  }

  @EventListener
  public void onIssueEvent(IssueEvent issueEvent) {
    Long eventTypeId = issueEvent.getEventTypeId();

    IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

    Issue issue = issueEvent.getIssue();

    CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

    CustomField cfSlaIteration = customFieldManager.getCustomFieldObjectByName( "SLA Iteration" );
    CustomField cfSlaElapsedTime = customFieldManager.getCustomFieldObjectByName( "SLA Elapsed Time" );
    CustomField cfSlaIterationHistory = customFieldManager.getCustomFieldObjectByName( "SLA Iteration History" );
    CustomField cfSlaLimit = customFieldManager.getCustomFieldObjectByName( "SLA Limit" );
    CustomField cfSlaStartDate = customFieldManager.getCustomFieldObjectByName( "SLA Activation" );
    CustomField cfSlaStatus = customFieldManager.getCustomFieldObjectByName( "SLA Status" );
    CustomField cfSlaPercentage = customFieldManager.getCustomFieldObjectByName( "SLA Percentage" );

    String issuetype = issue.getIssueTypeObject().getName();
    String priority = issue.getPriorityObject().getName();
    String status = issue.getStatusObject().getName();
    String project = issue.getProjectObject().getName();

    boolean validLicense = this.slaService.isLicenseValid();
    boolean checkSlaIteration = true;

    logger.info("License check in IssueListener ------------------");

    if (validLicense) {
      logger.info("SLA plugin License is VALID!");
    } else {
      logger.info("SLA plugin License is INVALID!");
    }

    SlaProject slaProject = this.slaService.getSlaProject(project);

    if ((slaProject != null) && (validLicense))
    {
      logger.info("SLA listener ------------------------------------------------------");

      String issue_details = "Issue Type " + issuetype + " with priority " + priority + " and in status " + status;

      SlaContract slaContract = this.slaService.getSlaContract(issuetype);

      if (slaContract != null) {

        long days = 1000 * 60 * 60 * 24;
        long hours = 1000 * 60 * 60;
        long minutes = 1000 * 60;

        long diff = 0;
        long iteration = 0;
        long iterationHistory = 0;
        long elapsedTime = 0;

        long slaIterationMil = 0;
        long slaIterationHistoryMil = 0;
        long slaLimitMil = 0;

        String slaLimit = "";
        String slaIteration = "";
        String slaIterationHistory = "";
        String slaElapsedTime = "";

        TimeReader timeReader =
                new TimeReader()
                        .addUnit("d", days)
                        .addUnit("h", hours)
                        .addUnit("m", minutes);

        String minsla = slaContract.getMinslatime();
        String maxsla = slaContract.getMaxslatime();

        String sla_details =  "SLA for " + issuetype + ": " + minsla + "->" + maxsla + " [id " + slaContract.getID() + "]";

        String cfSlaIterationValue = (String) issue.getCustomFieldValue( cfSlaIteration );
        String cfSlaElapsedTimeValue = (String) issue.getCustomFieldValue( cfSlaElapsedTime );
        String cfSlaIterationHistoryValue = (String) issue.getCustomFieldValue( cfSlaIterationHistory );
        String cfSlaLimitValue = (String) issue.getCustomFieldValue( cfSlaLimit );
        Object cfSlaStatusValue = issue.getCustomFieldValue( cfSlaStatus );
        Object cfSlaStartDateValue = issue.getCustomFieldValue( cfSlaStartDate );
        String cfSlaPercentageValue = (String) issue.getCustomFieldValue( cfSlaPercentage );

        SlaPriority slaPriority = this.slaService.getSlaPriority(priority);
        String priority_sla_details =  priority + " specific SLA: " + slaPriority.getSlatime();
        String project_details = "Statuses for In SLA: " + slaProject.getInsla() + " - and Out of SLA:" + slaProject.getOutsla();

        List<String> insla_list = Arrays.asList(slaProject.getInsla().split(","));
        List<String> outsla_list = Arrays.asList(slaProject.getOutsla().split(","));

        logger.info("SLA----------------------------------------------------------------");
        logger.info("{}", sla_details);
        logger.info("{}", priority_sla_details);
        logger.info("PROJECT------------------------------------------------------------");
        logger.info("{}", project_details);
        logger.info("{}", insla_list);
        logger.info("{}", outsla_list);
        logger.info("ISSUE--------------------------------------------------------------");
        logger.info("{} created at {}", issue.getKey(), issue.getCreated());
        logger.info("{}", issue_details);

        if (slaPriority != null && slaPriority.getMinmax().equals("Min")) {
            logger.info("Setting SLA to {} MIN {}", issuetype, minsla);
            cfSlaLimit.updateValue(null, issue, new ModifiedValue(cfSlaLimitValue, minsla), changeHolder);
            slaLimit = minsla;

        } else if (slaPriority != null && slaPriority.getMinmax().equals("Max")) {
            logger.info("Setting SLA to {} MAX {}", issuetype, maxsla);
            cfSlaLimit.updateValue(null, issue, new ModifiedValue(cfSlaLimitValue, maxsla), changeHolder);
            slaLimit = maxsla;

        } else if  ((slaPriority.getSlatime() != null) || (!slaPriority.getSlatime().equals(""))) {

            String pr_slatime = slaPriority.getSlatime();

            logger.info("Setting SLA to {} specific value {}", priority, pr_slatime);
            cfSlaLimit.updateValue(null, issue, new ModifiedValue(cfSlaLimitValue, pr_slatime), changeHolder);

            slaLimit = pr_slatime;
        }

        if (insla_list.contains(status))
        {
          logger.info("We have a MATCHING IN SLA status: {}", status);

          if (cfSlaStatusValue == null || !cfSlaStatusValue.equals("Out of SLA")) {
              cfSlaStatus.updateValue(null, issue, new ModifiedValue(cfSlaStatusValue, "In SLA"), changeHolder);
          }

          if (cfSlaStartDateValue == null) {
            Date d1 = new Date();
            Timestamp slaStart = new Timestamp(d1.getTime());
            logger.info("SLA re/start time {}", slaStart);

            cfSlaStartDate.updateValue(null, issue, new ModifiedValue(cfSlaStartDateValue, slaStart), changeHolder);
            cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, "0m"), changeHolder);
          }

          if (cfSlaIterationHistoryValue == null) {
              cfSlaIterationHistory.updateValue(null, issue, new ModifiedValue(cfSlaIterationHistoryValue, "n/a"), changeHolder);
          }
          if (cfSlaIterationValue == null) {
              cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, "0m"), changeHolder);
          }
          if (cfSlaElapsedTimeValue == null) {
              cfSlaElapsedTime.updateValue(null, issue, new ModifiedValue(cfSlaElapsedTimeValue, "0m"), changeHolder);
          }
        }
        //else if (outsla_list.contains(status)) {
        else {
          logger.info("We have an OUT of SLA status: {}", status);

          checkSlaIteration = false;

          if (cfSlaStartDateValue != null) {
              cfSlaStartDate.updateValue(null, issue, new ModifiedValue(cfSlaStartDateValue, null), changeHolder);
          }

          if ( cfSlaIterationHistoryValue != null && cfSlaIterationValue != null && cfSlaIterationHistoryValue.equals("n/a")) {
              cfSlaIterationHistory.updateValue(null, issue, new ModifiedValue(cfSlaIterationHistoryValue, cfSlaIterationValue), changeHolder);

              slaIteration = "0m";
              cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, slaIteration), changeHolder);

          } else  if ( cfSlaIterationHistoryValue != null && cfSlaIterationValue != null ) {

              slaIterationHistoryMil = timeReader.parse(cfSlaIterationHistoryValue);
              slaIterationMil = timeReader.parse(cfSlaIterationValue);

              iterationHistory = slaIterationHistoryMil + slaIterationMil;

              slaIterationHistory = convertToStringDuration (iterationHistory);

              cfSlaIterationHistory.updateValue(null, issue, new ModifiedValue(cfSlaIterationHistoryValue, slaIterationHistory), changeHolder);

              slaIteration = "0m";
              cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, slaIteration), changeHolder);

          }
        }

        cfSlaStatus.store();
        cfSlaLimit.store();
        cfSlaIteration.store();
        cfSlaIterationHistory.store();
        cfSlaElapsedTime.store();
        cfSlaStartDate.store();

        // fetch SLA start date value after update

        cfSlaIterationValue = (String) issue.getCustomFieldValue( cfSlaIteration );
        cfSlaIterationHistoryValue = (String) issue.getCustomFieldValue( cfSlaIterationHistory );
        cfSlaLimitValue = (String) issue.getCustomFieldValue( cfSlaLimit );
        cfSlaStartDateValue = issue.getCustomFieldValue( cfSlaStartDate );
        cfSlaPercentageValue = (String) issue.getCustomFieldValue( cfSlaPercentage );
        cfSlaElapsedTimeValue = (String) issue.getCustomFieldValue( cfSlaElapsedTime );

        if (checkSlaIteration == true && cfSlaStartDateValue != null) {

          Date d1 = new Date();
          Timestamp tsSlaStart = (Timestamp) issue.getCustomFieldValue( cfSlaStartDate );

          Date dateSlaStart = new Date(tsSlaStart.getTime());

          // calculate SLA iteration
          iteration = d1.getTime() - dateSlaStart.getTime();

          if (iteration > 0) {

            slaIteration = convertToStringDuration (iteration);

            // update SLA percentage
            if (  cfSlaIterationValue != null ) {

                logger.info("SLA Start Date {} current iteration {}",cfSlaStartDateValue, slaIteration);

                if ( cfSlaIterationHistoryValue != null && !cfSlaIterationHistoryValue.equals("n/a")) {
                    slaIterationHistoryMil = timeReader.parse(cfSlaIterationHistoryValue);
                    iterationHistory += slaIterationHistoryMil;
                }

                slaLimitMil =  timeReader.parse(cfSlaLimitValue);

                elapsedTime = iteration + iterationHistory;

                double percent = ((double) elapsedTime / (double) slaLimitMil) * 100.0;

                String slaPercentage = String.format("%3.0f%%", percent);

                cfSlaPercentage.updateValue(null, issue, new ModifiedValue(cfSlaPercentageValue, slaPercentage), changeHolder);
                cfSlaPercentage.store();

                if (percent >= 100.0) {
                    cfSlaStatus.updateValue(null, issue, new ModifiedValue(cfSlaStatusValue, "Out of SLA"), changeHolder);
                    cfSlaStatus.store();
                }

                logger.info("SLA Percentage {}", slaPercentage);
            }

            slaElapsedTime = convertToStringDuration (elapsedTime);

            cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, slaIteration), changeHolder);
            cfSlaElapsedTime.updateValue(null, issue, new ModifiedValue(cfSlaElapsedTimeValue, slaElapsedTime), changeHolder);

            cfSlaIteration.store();
            cfSlaElapsedTime.store();

          }
        }

        // set due date and requested date for publishing

        CustomField cfRequestedPublishDate = customFieldManager.getCustomFieldObjectByName( "Requested date for publishing" );

        if ((cfRequestedPublishDate != null) && (cfSlaLimit != null)) {

            if (slaLimit!= null) {
                slaLimitMil = timeReader.parse(slaLimit);

                if (slaLimitMil > 0) {
                    MutableIssue slaIssue = (MutableIssue) issue;

                    Calendar cal = Calendar.getInstance();
                    Timestamp dueDate = new Timestamp(cal.getTimeInMillis() + slaLimitMil);

                    slaIssue.setDueDate(dueDate);

                }
            }
        }

        Date dueDate = issue.getDueDate();

        if ((cfRequestedPublishDate != null) && (dueDate != null)) {
            Object cfRequestedPublishDateValue = issue.getCustomFieldValue( cfRequestedPublishDate );

            // logger.info("Due Date: {}", dueDate);

            Calendar rCal = Calendar.getInstance();
            rCal.setTime(dueDate);
            // logger.info("rCal (due date): {}", rCal);

            rCal.add(Calendar.DATE,1);

            Date rDate = rCal.getTime();

            // logger.info("rCal (due date + 1: {} rDate: {}", rCal, rDate);

            Timestamp requestedDate = new Timestamp(rDate.getTime());

            logger.info("Due Date: {} Requested date for publishing: {}", dueDate.toString(), requestedDate);

            cfRequestedPublishDate.updateValue(null, issue, new ModifiedValue(cfRequestedPublishDateValue, requestedDate), changeHolder);
            cfRequestedPublishDate.store();

        }

      }
    }

    logger.info("-------------------------------------------------------------------");

    if (eventTypeId.equals(EventType.ISSUE_CREATED_ID)) {
      logger.info("CREATED------------------------------------------------------------");
      logger.info("Issue {} has been created at {}", issue.getKey(), issue.getCreated());
      logger.info("Summary: {}", issue.getSummary());



      logger.info("-------------------------------------------------------------------");
    } else if (eventTypeId.equals(EventType.ISSUE_UPDATED_ID)) {

      logger.info("UPDATED------------------------------------------------------------");
      logger.info("Issue {} has been updated at {}", issue.getKey(), issue.getUpdated());
      logger.info("Summary: {}", issue.getSummary());
      logger.info("-------------------------------------------------------------------");

    } else if (eventTypeId.equals(EventType.ISSUE_RESOLVED_ID)) {
      logger.info("COMMENTED----------------------------------------------------------");
      logger.info("Issue: {} has been resolved at {}", issue.getKey(), issue.getResolutionDate());
      logger.info("-------------------------------------------------------------------");
    } else if (eventTypeId.equals(EventType.ISSUE_CLOSED_ID)) {
      logger.info("CLOSED------------------------------------------------------------");
      logger.info("Issue: {} has been closed at {}", issue.getKey(), issue.getUpdated());
      logger.info("-------------------------------------------------------------------");
    }
  }

}
