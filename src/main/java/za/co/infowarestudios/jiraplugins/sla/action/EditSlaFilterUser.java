package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.user.UserUtils;
import com.atlassian.crowd.embedded.api.User;

import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collection;

public class EditSlaFilterUser extends SlaFilterUserAction {

    public EditSlaFilterUser(SlaService slaService) {
        super(slaService);
    }

    public Collection<User> getAllSystemUsers() {
      return UserUtils.getAllUsers();
    }

    @Override
    protected String doExecute() throws Exception {

      //final String filteruser = request.getParameter("filteruser");
      final String filteruser = ((String[]) ActionContext.getParameters().get("filteruser"))[0];

      slaFilterUser.setFilteruser(filteruser);

      slaFilterUser.save();

      return getRedirect(VIEW_FILTERUSER_PAGE);
    }

    public String doEdit() throws Exception {
        doKeyValidation();
        return INPUT;
    }

    @Override
    public void doValidation() {
        doKeyValidation();
        doFieldValidation();
    }


}
