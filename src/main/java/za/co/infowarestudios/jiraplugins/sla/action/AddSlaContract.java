package za.co.infowarestudios.jiraplugins.sla.action;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.issuetype.IssueType;
import webwork.action.ActionContext;
import za.co.infowarestudios.jiraplugins.sla.model.SlaContract;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;

import java.util.Collection;

public class AddSlaContract extends SlaContractAction {

  public AddSlaContract(SlaService slaService) {
    super(slaService);
  }

  public Collection<IssueType> getAllIssueTypes() {
    Collection<IssueType> allIssueTypes = ComponentAccessor.getConstantsManager().getAllIssueTypeObjects();
    return allIssueTypes;
  }

  @Override
  public String doExecute() throws Exception {

    // request has been deprecated
    // final String description = request.getParameter("description");
    // final String issuetype = request.getParameter("issuetype");
    // final String minslatime = request.getParameter("minslatime");
    // final String maxslatime = request.getParameter("maxslatime");


    final String description = ((String[])ActionContext.getParameters().get("description"))[0];
    final String issuetype = ((String[])ActionContext.getParameters().get("issuetype"))[0];
    final String minslatime = ((String[])ActionContext.getParameters().get("minslatime"))[0];
    final String maxslatime = ((String[])ActionContext.getParameters().get("maxslatime"))[0];

    SlaContract slaContract = this.slaService.createSlaContract(issuetype, description, minslatime, maxslatime);

    return getRedirect(VIEW_CONTRACT_PAGE);
  }

  @Override
  public void doValidation() {
    doFieldValidation();
  }

  public String doAdd() throws Exception {
    return INPUT;
  }

}
