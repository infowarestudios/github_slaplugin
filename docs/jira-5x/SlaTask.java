package za.co.infowarestudios.jiraplugins.sla.scheduling;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.UserUtils;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.mail.server.SMTPMailServer;
import org.apache.commons.lang.StringUtils;
import za.co.infowarestudios.jiraplugins.sla.model.SlaFilterUser;
import za.co.infowarestudios.jiraplugins.sla.model.SlaProject;
import za.co.infowarestudios.jiraplugins.sla.model.SlaEmail;
import za.co.infowarestudios.jiraplugins.sla.service.SlaService;
import za.co.infowarestudios.jiraplugins.sla.util.GlobalSettings;

import com.atlassian.jira.issue.Issue;

import com.atlassian.jira.component.ComponentAccessor;

import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.query.Query;

import com.atlassian.jira.security.JiraAuthenticationContext;

import com.atlassian.jira.web.bean.PagerFilter;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import za.co.infowarestudios.jiraplugins.sla.util.TimeReader;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.scheduling.PluginJob;

// import com.atlassian.jira.user.ApplicationUser;
// import com.atlassian.jira.issue.watchers.WatcherManager;

public class SlaTask implements PluginJob {

  private final static Logger logger = LoggerFactory.getLogger(SlaTask.class);

  @Override
  public void execute(Map<String, Object> jobDataMap) {

    UserManager userManager = ComponentAccessor.getUserManager();
    GroupManager groupManager = ComponentAccessor.getGroupManager();
    IssueManager issueManager = ComponentAccessor.getIssueManager();

    ProjectRoleManager projectRoleManager = ComponentManager.getComponentInstanceOfType(ProjectRoleManager.class);

    long diffMinutes = 0;
    long diffHours = 0;
    long diffDays = 0;
    String slaDuration;
    String slaNotification;

    boolean sendEmail = false;
    String emailSubject = "";

    SlaMonitor monitor = (SlaMonitor) jobDataMap.get("SlaMonitorImpl:instance");
    SlaService service = (SlaService) jobDataMap.get("SlaService");

    DateTime now = DateTime.now();
    SimpleDateFormat fmt = GlobalSettings.createHunDateFormatter();

    monitor.setLastRun(now.toDate());
    monitor.setNextRun(now.plus(monitor.getInterval()).toDate());

    logger.info("SCHEDULER----------------------------------------------------------");
    logger.info("SLA Scheduler executed at " + fmt.format(monitor.getLastRun()) + " and will execute again at "
              + fmt.format(monitor.getNextRun()));

    logger.info("License check in Scheduler  ------------------");
    boolean validLicense = service.isLicenseValid();

    if (validLicense) {
      logger.info("SLA plugin License is VALID!");
    } else {
      logger.info("SLA plugin License is INVALID!");
    }

    SlaFilterUser slaFilterUser = service.getFirstSlaFilterUser();

    User user =  null;

    if (slaFilterUser != null){
      String filteruser = slaFilterUser.getFilteruser();
      logger.info("Filter User: " + filteruser);

      user = UserUtils.getUser(filteruser);

      JiraAuthenticationContext jiraAuthenticationContext = monitor.getJiraAuthenticationContext();

      jiraAuthenticationContext.setLoggedInUser(user);

    }

    CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

    CustomField cfSlaIteration= customFieldManager.getCustomFieldObjectByName( "SLA Iteration" );
    CustomField cfSlaLimit= customFieldManager.getCustomFieldObjectByName( "SLA Limit" );
    CustomField cfSlaStartDate= customFieldManager.getCustomFieldObjectByName( "SLA Activation" );
    CustomField cfSlaPercentage= customFieldManager.getCustomFieldObjectByName( "SLA Percentage" );
    CustomField cfSlaStatus= customFieldManager.getCustomFieldObjectByName( "SLA Status" );
    CustomField cfSlaNotifications= customFieldManager.getCustomFieldObjectByName( "SLA Notifications" );
    CustomField cfSlaTotalDuration= customFieldManager.getCustomFieldObjectByName( "SLA Total Duration" );

    JqlClauseBuilder builder = JqlQueryBuilder.newClauseBuilder();

    //builder.customField(cfSlaStatus.getIdAsLong()).like("In SLA").not().customField(cfSlaStartDate.getIdAsLong()).eqEmpty();
    builder.customField(cfSlaStatus.getIdAsLong()).like("In SLA");

    Query query = builder.buildQuery();

    logger.info("QUERY--------------------------------------------------------------");
    // logger.info("{}", appUser);
    logger.info("{}", query);

    try {
      SearchProvider searchProvider = ComponentAccessor.getComponentOfType(SearchProvider.class);

      SearchResults searchResults = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter());

      List<Issue> issues = searchResults.getIssues();

      logger.info("step #1 - showing issues under SLA");

      logger.info("{}", issues);

      for (Issue issue : issues) {

            sendEmail = false;

            String status = issue.getStatusObject().getName();

            Object cfSlaStartDateValue = issue.getCustomFieldValue( cfSlaStartDate );
            String cfSlaStatusValue = (String) issue.getCustomFieldValue( cfSlaStatus );
            String cfSlaNotificationsValue = (String) issue.getCustomFieldValue( cfSlaNotifications );

            String cfSlaIterationValue = (String) issue.getCustomFieldValue( cfSlaIteration );
            String cfSlaLimitValue = (String) issue.getCustomFieldValue( cfSlaLimit );
            String cfSlaPercentageValue = (String) issue.getCustomFieldValue( cfSlaPercentage );
            String cfSlaTotalDurationValue = (String) issue.getCustomFieldValue( cfSlaTotalDuration );

            Project project = issue.getProjectObject();

            SlaProject slaProject = service.getSlaProject(project.getName());

            List<String> insla_list = Arrays.asList(slaProject.getInsla().split(","));
            List<String> outsla_list = Arrays.asList(slaProject.getOutsla().split(","));

            String issuetype = issue.getIssueTypeObject().getName();
            String priority = issue.getPriorityObject().getName();
            String issue_details = "Issue Type " + issuetype + " with priority " + priority + " and in status " + status;

            logger.info("SLA DETAILS -> ISSUE-----------------------------------------------");
            logger.info("{}", issue.getKey());
            logger.info("{} created at {}", issue_details, issue.getCreated());

            logger.info("SLA Status -> {}", cfSlaStatusValue);
            logger.info("SLA Start Date -> {}", cfSlaStartDateValue);
            logger.info("SLA Duration -> {}", cfSlaIterationValue);
            logger.info("SLA Limit -> {}", cfSlaLimitValue);
            logger.info("SLA Percentage -> {}", cfSlaPercentageValue);

            if ( (cfSlaStatusValue != null) && (insla_list.contains(status) && cfSlaStatusValue.equals("In SLA") && cfSlaStartDateValue != null ))   {

                logger.info("SLA UPDATE -> ISSUE------------------------------------------------");
                logger.info("{}", issue.getKey());
                logger.info("step #2");

                IssueChangeHolder changeHolder = new DefaultIssueChangeHolder();

                long days = 1000 * 60 * 60 * 24;
                long hours = 1000 * 60 * 60;
                long minutes = 1000 * 60;

                long totalDuration = 0;
                long diff = 0;
                long slaDurationMil = 0;
                long slaLimitMil = 0;
                double percent =  0.0;
                long slaProgressMil = 0;

                TimeReader timeReader =
                        new TimeReader()
                                .addUnit("d", days)
                                .addUnit("h", hours)
                                .addUnit("m", minutes);

                Date d1 = new Date();
                Timestamp tsSlaStart = (Timestamp) issue.getCustomFieldValue( cfSlaStartDate );

                Date dateSlaStart = new Date(tsSlaStart.getTime());

                totalDuration = d1.getTime() - dateSlaStart.getTime();

                diffMinutes = totalDuration / (60 * 1000) % 60;
                diffHours = totalDuration / (60 * 60 * 1000) % 24;
                diffDays = totalDuration / (24 * 60 * 60 * 1000);

                if (totalDuration > 0) {

                    logger.info("diffMinutes > 0");

                    slaDuration = diffDays > 0 ? diffDays + "d " : "";
                    slaDuration += diffHours > 0 ? diffHours + "h " : "";
                    slaDuration += diffMinutes > 0 ? diffMinutes + "m" : "";

                    cfSlaIteration.updateValue(null, issue, new ModifiedValue(cfSlaIterationValue, slaDuration), changeHolder);

                    logger.info("step #3");
                    if (cfSlaStartDateValue != null) {
                        logger.info("SLA VALUES---------------------------------------------------------");
                        logger.info("SLA Start Date {} duration {}",cfSlaStartDateValue, slaDuration);
                    }

                    cfSlaIteration.store();

                    cfSlaIterationValue = slaDuration;
                }


                if (  cfSlaIterationValue != null )   {
                    logger.info("cfSlaIterationValue != null");

                    slaDurationMil = timeReader.parse(cfSlaIterationValue);
                    slaLimitMil =  timeReader.parse(cfSlaLimitValue);

                    if ( cfSlaTotalDurationValue != null && !cfSlaTotalDurationValue.equals("Started")) {
                        slaProgressMil = timeReader.parse(cfSlaTotalDurationValue);
                        totalDuration += slaProgressMil;
                    }

                    percent = ((double) totalDuration / (double) slaLimitMil) * 100.0;

                    String slaPercentage = String.format("%3.0f%%", percent);

                    logger.info("SLA Percentage {}", slaPercentage);

                    cfSlaPercentage.updateValue(null, issue, new ModifiedValue(cfSlaPercentageValue, slaPercentage), changeHolder);
                    cfSlaPercentage.store();

                    logger.info("step #4 - testing notifications");

                    if ((percent >= 50.0) && (percent < 100.0) ){
                      if ((cfSlaNotificationsValue == null) || ((cfSlaNotificationsValue != null) && (!cfSlaNotificationsValue.equals("50% notification sent")))) {
                        logger.info("step #5 - sending 50%");
                        sendEmail = true;
                        slaNotification = "50% notification sent";
                        cfSlaNotifications.updateValue(null, issue, new ModifiedValue(cfSlaNotificationsValue, slaNotification), changeHolder);
                        emailSubject = "50% + SLA reached on " + issue.getKey() + "_" + issue.getSummary();
                      }
                    }  else if (percent >= 100.0) {
                      if ((cfSlaNotificationsValue == null) || ((cfSlaNotificationsValue != null) && (!cfSlaNotificationsValue.equals("100% notification sent")))) {
                        sendEmail = true;
                        logger.info("step #5 - sending 100%");
                        slaNotification = "100% notification sent";
                        cfSlaNotifications.updateValue(null, issue, new ModifiedValue(cfSlaNotificationsValue, slaNotification), changeHolder);
                        emailSubject = "100% + SLA reached on " + issue.getKey() + "_" + issue.getSummary();
                      }
                    }

                    if (slaDurationMil > slaLimitMil) {
                        cfSlaStatus.updateValue(null, issue, new ModifiedValue(cfSlaStatusValue, "Out of SLA"), changeHolder);
                        cfSlaStatus.store();
                        sendEmail = true;

                        emailSubject = "Out SLA status reached on " + issue.getKey() + " " + issue.getSummary();
                    }

                    logger.info("step #6");

                    diff = slaDurationMil - slaLimitMil;
                    if (diff > 0) {

                        logger.info("diff > 0");

                        diffMinutes = diff / (60 * 1000) % 60;
                        diffHours = diff / (60 * 60 * 1000) % 24;
                        diffDays = diff / (24 * 60 * 60 * 1000);

                        slaDuration = diffDays > 0 ? diffDays + "d " : "";
                        slaDuration += diffHours > 0 ? diffHours + "h " : "";
                        slaDuration += diffMinutes > 0 ? diffMinutes + "m" : "";

                        logger.info("SLA Difference {} {}", slaDuration, timeReader.parse(cfSlaIterationValue) -  timeReader.parse(cfSlaLimitValue));
                    }

                    logger.info("email - step #1");

                    SlaEmail slaEmail = service.getSlaEmail(project.getName());

                    if (slaEmail == null)  {
                      logger.info("EMAIL SETTINGS-----------------------------------------------------");
                      logger.info("      No email settings were specified.");
                    }

                    logger.info("email - step #2");

                    if ((slaEmail != null) && (sendEmail)) {

                      List<String> listOfRecipients = new ArrayList<String>();

                      listOfRecipients.add("johan.pretorius+sla@gmail.com");

                      String email_user = slaEmail.getUser();
                      String email_group = slaEmail.getGroup();
                      String email_role = slaEmail.getRole();
                      boolean email_reporter = slaEmail.getReporter();
                      boolean email_component_lead = slaEmail.getComponentLead();
                      boolean email_all_watchers = slaEmail.getAllWatchers();

                      String address = "";

                      StringBuilder selected_builder = new StringBuilder();

                      String email_booleans = "Selected: ";

                      selected_builder.append(email_booleans);

                        if(email_group != null && !email_group.isEmpty())  {

                        logger.info("step #5 - sending to group");
                        selected_builder.append("Group | ");

                        Group group = userManager.getGroup(email_group);
                        Collection<User> usersInGroup = groupManager.getUsersInGroup(group);
                        for (User group_user : usersInGroup){
                          address = group_user.getEmailAddress();
                          listOfRecipients.add(address);
                        }
                      }

                      if(email_user != null && !email_user.isEmpty())  {

                        logger.info("step #5 - sending to user");
                        selected_builder.append("User | ");
                        User individual_user = UserUtils.getUser(email_user);
                        address = individual_user.getEmailAddress();
                        listOfRecipients.add(address);
                      }

                      if(email_role != null && !email_role.isEmpty())  {

                        logger.info("step #5 - sending to role");
                        selected_builder.append("Role | ");
                        ProjectRole role = projectRoleManager.getProjectRole(email_role);
                        ProjectRoleActors projectActors = projectRoleManager.getProjectRoleActors(role, project);
                        Collection<User> actors = projectActors.getUsers();
                        for (User actor : actors){
                          address = actor.getEmailAddress();
                          listOfRecipients.add(address);
                        }
                      }

                      if (email_reporter) {
                        logger.info("step #5 - sending to reporter");
                        User reporter = issue.getReporter();
                        address = reporter.getEmailAddress();
                        listOfRecipients.add(address);

                        selected_builder.append("Reporter | ");
                      }

                      if (email_all_watchers) {

                        logger.info("step #5 - sending to watchers");
                        // 6.x
                        // Collection<ApplicationUser> watchers = issueManager.getWatchersFor(issue);
                        //  for (ApplicationUser watcher : watchers){

                        // 5.2.10
                        Collection<User> watchers = issueManager.getWatchers(issue);

                        for (User watcher : watchers){
                          address = watcher.getEmailAddress();
                          listOfRecipients.add(address);
                        }

                        selected_builder.append("All Watchers");
                      }

                      String email_details = selected_builder.toString();

                      logger.info("EMAIL SETTINGS-----------------------------------------------------");
                      logger.info("      {}" , email_details);

                      User reporter = issue.getReporter();

                      // 6.x
                      // ApplicationUser projectManager = project.getProjectLead();

                      // 5.x
                      User projectManager = project.getLead();

                      String pmEmail = projectManager.getEmailAddress();

                      // configure email
                      String baseURL = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
                      String issueURL = baseURL + "/browse/" + issue.getKey();

                      logger.info("SENDING MAIL-------------------------------------------------------");
                      logger.info ("Sending {} to: {}", emailSubject, pmEmail);
                      logger.info ("Base URL: {}", baseURL);
                      logger.info ("Issue URL: {}", issueURL);

                      MailServerManager mailServerManager = ComponentAccessor.getMailServerManager();
                      SMTPMailServer smtp = mailServerManager.getDefaultSMTPMailServer();

                      Email email = new Email(pmEmail);

                      String cc_list = StringUtils.join(listOfRecipients, ",");

                      logger.info ("Email CC List: {}", cc_list);

                      email.setCc(cc_list);

                      email.setFrom(smtp.getDefaultFrom());
                      email.setSubject(emailSubject);

                      String emailText = slaPercentage + " SLA reached on " + issue.getKey() + " " + issue.getSummary();

                      email.setBody(emailText);
                      email.setMimeType("text/html");
                      logger.info ("email to be sent {}", email);

                      // send email
                      SingleMailQueueItem item = new SingleMailQueueItem(email);

                      logger.info ("item placed on queue {}", item);

                      try {
                          ComponentAccessor.getMailQueue().addItem(item);
                          logger.info ("Email has been queued...");
                      } catch (Exception e) {
                          logger.warn("ERROR SENDING SLA EMAIL", e);
                      }

                    }

                }

              logger.info("-------------------------------------------------------------------");
            }
        }

    } catch (SearchException e) {
        logger.error("SearchException :"+e.getMessage());
    }

  }
}