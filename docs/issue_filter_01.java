package eventim.services

import com.atlassian.crowd.embedded.api.User
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.web.bean.PagerFilter
import org.apache.log4j.Logger
import static com.atlassian.jira.event.type.EventDispatchOption.DO_NOT_DISPATCH

Logger log = Logger.getLogger("EscalationService")

log.debug "Starting Escalation Service"

IssueService issueService = ComponentAccessor.issueService
user = ComponentAccessor.userUtil.getUserObject('jira_bot') // This has to be a user who has access to all issues

List<Issue> issues = getFilterResult("duedate < 14d AND priority not in (Blocker, Critical) and resolution = Unresolved", log, user)
log.debug "Found ${issues.size()} issues to raise priority."
issues.each {issue ->
    IssueInputParameters issueInputParameters = issueService.newIssueInputParameters()
    issueInputParameters.setPriorityId('2') // Critical
    validationResult = issueService.validateUpdate(user, issue.id, issueInputParameters)

    if (validationResult.isValid()) {
        updateResult = issueService.update(user, validationResult, DO_NOT_DISPATCH, false)
        if (!updateResult.isValid()) {
            log.error "Update failed"
            updateResult.errorCollection.errorMessages.each {log.error "Error Message: $it"}
        } else {
            log.info "Update priority of issue with ID $issue.id"
        }
    } else {
        log.error "validationResult for ${issue.id} with user ${user} is not valid, not attempting update."
        validationResult.errorCollection.errorMessages.each {log.error "Error Message: $it"}
    }
}

List<Issue> getFilterResult(String jqlSearch, Logger log, User user) {
    SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
    List<Issue> issues = null

    SearchService.ParseResult parseResult =  searchService.parseQuery(user, jqlSearch)
    if (parseResult.isValid()) {
        def searchResult = searchService.search(user, parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
        issues = searchResult.issues
    } else {
        log.error("Invalid JQL: " + jqlSearch)
    }
    return issues
}

