package za.co.infowarestudios.jiraplugins.sla.listener;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.managers.DefaultCustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.fields.CustomField;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import org.ofbiz.core.entity.GenericValue;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

public class PluginListener implements InitializingBean, DisposableBean {

    private static final String SLA_ITERATION = "SLA Iteration";
    private static final String SLA_ITERATION_HISTORY = "SLA Iteration History";
    private static final String SLA_ELAPSED_TIME = "SLA Elapsed Time";
    private static final String SLA_ACTIVATION = "SLA Activation";
    private static final String SLA_LIMIT = "SLA Limit";
    private static final String SLA_PERCENTAGE = "SLA Percentage";
    private static final String SLA_STATUS = "SLA Status";
    private static final String SLA_NOTIFICATION = "SLA Notifications";

    private final CustomFieldManager customFieldManager;
    private final FieldScreenManager fieldScreenManager;

    public PluginListener(CustomFieldManager customFieldManager, FieldScreenManager fieldScreenManager) {
        this.customFieldManager = checkNotNull(customFieldManager);
        this.fieldScreenManager = checkNotNull(fieldScreenManager);
    }


  @Override
  public void destroy() throws Exception {

  }

   private void deprecated_destroy() throws Exception {

        CustomField cField = null;

        cField = this.customFieldManager.getCustomFieldObjectByName(SLA_LIMIT);
        if (cField != null) {
            this.customFieldManager.removeCustomField(cField);
        }

        cField = this.customFieldManager.getCustomFieldObjectByName(SLA_ITERATION);
        if (cField != null) {
            this.customFieldManager.removeCustomField(cField);
        }

       cField = this.customFieldManager.getCustomFieldObjectByName(SLA_ITERATION_HISTORY);
       if (cField != null) {
           this.customFieldManager.removeCustomField(cField);
       }

        cField = this.customFieldManager.getCustomFieldObjectByName(SLA_PERCENTAGE);
        if (cField != null) {
            this.customFieldManager.removeCustomField(cField);
        }

        cField = this.customFieldManager.getCustomFieldObjectByName(SLA_STATUS);
        if (cField != null) {
            this.customFieldManager.removeCustomField(cField);
        }

        cField = this.customFieldManager.getCustomFieldObjectByName(SLA_ACTIVATION);
        if (cField != null) {
            this.customFieldManager.removeCustomField(cField);
        }

    }


  @Override
  public void afterPropertiesSet() throws Exception {

        CustomField currentField = null;
        CustomField cField = null;

        FieldScreen defaultScreen = fieldScreenManager.getFieldScreen(FieldScreen.DEFAULT_SCREEN_ID);

        //Create a list of issue types for which the custom field needs to be available
        List<GenericValue> issueTypes = new ArrayList<GenericValue>();
        issueTypes.add(null);

        //Create a list of project contexts for which the custom field needs to be available
        List<JiraContextNode> contexts = new ArrayList<JiraContextNode>();
        contexts.add(GlobalIssueContext.getInstance());

        //Add SLA Limit custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_LIMIT);
        if (currentField == null) {
            cField = this.customFieldManager.createCustomField(SLA_LIMIT, "SLA Limit",
                    this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                    this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                    contexts, issueTypes);

            // Add field to default Screen
            if (!defaultScreen.containsField(cField.getId())) {
                FieldScreenTab firstTab = defaultScreen.getTab(0);
                firstTab.addFieldScreenLayoutItem(cField.getId());
            }
        }

        //Add SLA Iteration custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_ITERATION);
        if (currentField == null) {
            cField = this.customFieldManager.createCustomField(SLA_ITERATION, "SLA Iteration",
                    this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                    this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                    contexts, issueTypes);

            // Add field to default Screen
            if (!defaultScreen.containsField(cField.getId())) {
                FieldScreenTab firstTab = defaultScreen.getTab(0);
                firstTab.addFieldScreenLayoutItem(cField.getId());
            }
        }

        //Add SLA Elapsed Time custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_ELAPSED_TIME);
        if (currentField == null) {
          cField = this.customFieldManager.createCustomField(SLA_ELAPSED_TIME, "SLA Elapsed Time",
                  this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                  this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                  contexts, issueTypes);

          // Add field to default Screen
          if (!defaultScreen.containsField(cField.getId())) {
              FieldScreenTab firstTab = defaultScreen.getTab(0);
              firstTab.addFieldScreenLayoutItem(cField.getId());
          }
        }

        //Add SLA In ITERATION_HISTORY custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_ITERATION_HISTORY);
        if (currentField == null) {
          cField = this.customFieldManager.createCustomField(SLA_ITERATION_HISTORY, "SLA Total Duration",
                  this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                  this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                  contexts, issueTypes);

          // Add field to default Screen
          if (!defaultScreen.containsField(cField.getId())) {
              FieldScreenTab firstTab = defaultScreen.getTab(0);
              firstTab.addFieldScreenLayoutItem(cField.getId());
          }
        }

        //Add SLA Percentage custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_PERCENTAGE);
        if (currentField == null) {
            cField = this.customFieldManager.createCustomField(SLA_PERCENTAGE, "SLA Percentage",
                    this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                    this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                    contexts, issueTypes);

            // Add field to default Screen
            if (!defaultScreen.containsField(cField.getId())) {
                FieldScreenTab firstTab = defaultScreen.getTab(0);
                firstTab.addFieldScreenLayoutItem(cField.getId());
            }
        }

        //Add SLA Status custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_STATUS);
        if (currentField == null) {
            cField = this.customFieldManager.createCustomField(SLA_STATUS, "SLA Status",
                    this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
                    this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
                    contexts, issueTypes);

            // Add field to default Screen
            if (!defaultScreen.containsField(cField.getId())) {
                FieldScreenTab firstTab = defaultScreen.getTab(0);
                firstTab.addFieldScreenLayoutItem(cField.getId());
            }
        }

      //Add SLA Notification custom field
      currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_NOTIFICATION);
      if (currentField == null) {
        cField = this.customFieldManager.createCustomField(SLA_NOTIFICATION, "SLA Notifications",
            this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield"),
            this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:textsearcher"),
            contexts, issueTypes);

        // Add field to default Screen
        if (!defaultScreen.containsField(cField.getId())) {
          FieldScreenTab firstTab = defaultScreen.getTab(0);
          firstTab.addFieldScreenLayoutItem(cField.getId());
        }
      }

        //Add SLA Activation custom field
        currentField = this.customFieldManager.getCustomFieldObjectByName(SLA_ACTIVATION);
        if (currentField == null) {
            cField = this.customFieldManager.createCustomField(SLA_ACTIVATION, "SLA Activation",
                    this.customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:datetime"),
                    this.customFieldManager.getCustomFieldSearcher("com.atlassian.jira.plugin.system.customfieldtypes:DateRangesearcher"),
                    contexts, issueTypes);

            // Add field to default Screen
            if (!defaultScreen.containsField(cField.getId())) {
                FieldScreenTab firstTab = defaultScreen.getTab(0);
                firstTab.addFieldScreenLayoutItem(cField.getId());
            }
        }


    }
}

